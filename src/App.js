import { useRef, useState, useEffect, useCallback } from "react"
import "./App.css"

import LeafImg from "./leaves.jpg"

function App() {
	const imgRef = useRef(null)
	const resultRef = useRef(null)
	const canvasRef = useRef(null)

	const [listColor, setListColor] = useState([])
	const [imgSrc, setImgSrc] = useState(null)
	const hiddenFileInput = useRef(null)

	const toHex = n => {
		n = parseInt(n, 10)
		console.log(n)
		if (isNaN(n)) return "00"
		n = Math.max(0, Math.min(n, 255))
		return "0123456789ABCDEF".charAt((n - (n % 16)) / 16) + "0123456789ABCDEF".charAt(n % 16)
	}

	const rgbToHex = useCallback((r, g, b) => {
		return `#${toHex(r)}${toHex(g)}${toHex(b)}`
	}, [])

	const canvasUtil = (el, image, callback) => {
		el.width = image.width
		el.height = image.height
		// draw image in canvas tag
		el.getContext("2d").drawImage(image, 0, 0, image.width, image.height)
		return callback()
	}

	const resetColor = () => {
		setListColor([])
	}

	useEffect(() => {
		function onImgClick(e) {
			let x = ""
			let y = ""
			if (e.offsetX) {
				x = e.offsetX
				y = e.offsetY
			}
			// firefox
			else if (e.layerX) {
				x = e.layerX
				y = e.layerY
			}
			canvasUtil(canvasRef.current, imgRef.current, function () {
				// get image data
				var p = canvasRef.current.getContext("2d").getImageData(x, y, 1, 1).data
				if (listColor.length <= 3) {
					const tempListColor = [...listColor]
					tempListColor.push(rgbToHex(p[0], p[1], p[2]))
					setListColor(tempListColor)
				}
			})
		}
		imgRef.current.addEventListener("click", e => onImgClick(e))
		return imgRef.current.removeEventListener("click", onImgClick)
	}, [listColor, rgbToHex])

	const renderColorBox = () => {
		return listColor.map(color => {
			return <div key={color} style={{ width: 20, height: 20, background: color }} />
		})
	}

	const onImgUploadClick = () => {
		hiddenFileInput.current.click()
	}

	const handleImgChange = e => {
		const imgSrc = e.target.files[0]
		setImgSrc(imgSrc)
	}

	return (
		<>
			<div className='thumbnail'>
				<img alt='' ref={imgRef} crossOrigin='anonymous' src={imgSrc ? URL.createObjectURL(imgSrc) : LeafImg} />
				<input type='file' id='img-upload' ref={hiddenFileInput} onChange={handleImgChange} />
				<button className='upload-button' onClick={onImgUploadClick}>
					upload
				</button>
			</div>
			<div className='result' ref={resultRef}></div>
			<canvas id='canvas-ja' ref={canvasRef}></canvas>
			{renderColorBox()}
			{listColor.length === 3 && <button onClick={resetColor}>reset</button>}
		</>
	)
}

export default App
